
\subsection*{Conclusiones}

  Los radares perfiladores de vientos en UHF han probado ser instrumentos valiosos para el registro del comportamiento de la atmósfera, desde la medición diaria del viento y turbulencias hasta el registro en largo plazo de vientos, precipitaciones y clima extremo. Por lo tanto, pueden ayudar a la prevención de desastres naturales.

  \gls{claire} es el nombre del radar perfilador de vientos en UHF diseñado por el ROJ y que entrará en pruebas a mediados del año 2017. Éste es sensible tanto a ecos del aire claro como a precipitaciones. Los retornos de eco pueden ser separados mediante análisis espectral en frecuencia (\gls{fsa}) y mediante técnicas de antenas espaciadas.

  El diseño de \gls{claire} incluye módulos de generación de señal, conversión de frecuencia, amplificación, filtrado y referencia de reloj. Muchos de estos equipos, conocimientos y tecnologías han sido reutilizados o adaptados para operar en 445 MHz a partir de proyectos previos del \gls{roj}, los cuales facilitaron y aceleraron el desarrollo e implementación del prototipo.

  Gracias a ello, la presente tesis permite concluir que:

\begin{itemize}

  \item Se ha analizado, diseñado y probado en laboratorio los módulos generación de señal, módulos de conversión de frecuencia, amplificación y filtrado tanto para transmisión como para recepción, así como el módulo de generación de señales de reloj para un radar que opera 445 MHz.

  \item Se han recogido los requisitos y especificaciones, tal y como se muestra en la Tabla \ref{tabla_requisitos_01}.

  \item Se concluye que las especificaciones y requisitos no suelen estar completos y definidos en su totalidad al principio del trabajo. Para lograr una recopilación óptima de requisitos se ha formulado una metodología de entrevistas y reuniones iterativas, donde los parámetros y especificaciones son mostrados gráficamente en un diagrama de bloques.

  \item En base a lo anterior, se logró determinar qué equipos del \gls{roj} van a ser adaptados (Tabla \ref{tabla_requisitos_03}). Se concluye la necesidad de conocer en detalle la electrónica y funcionalidad de estos equipos, ya que el diseño original puede no funcionar bien para el nuevo diseño.

  \item Se ha logrado establecer un diagrama inicial de bloques donde se describen las etapas del sistema (Figura \ref{diagrama_radar_uhf_bloques_detallado}). Este diagrama ha servido para definir los parámetros de funcionamiento.

  \item En la Figura \ref{diagrama_radar_uhf_bloques_detallado} se ha determinado los parámetros y especificaciones de las señales de reloj y de radio frecuencia (RF). Éstos parámetros se obtienen luego de definir qué elementos conforman el diseño y cómo estos interaccionan entre sí.

  \item Se han implementado pruebas de laboratorio para validar el diseño de las etapas de generación, conversión de frecuencia, amplificación y filtrado tanto de transmisión como de recepción de las señales de RF, como se detalla en el Capítulo 4.

  \item Se han logrado realizar los ajustes para que los niveles de señal sean los correctos tanto para la etapa de transmisión, potencia y antena como para la etapa de recepción, adquisición y procesamiento de señales, tal y como se señala en el diagrama final del diseño (Figura \ref{diagrama_radar_uhf_bloques}).

  \item Finalmente se concluye la importancia de los niveles de señal adecuados. Niveles bajos no entregan buena relación señal a ruido. Niveles altos suelen saturar el sistema o generar señales adicionales no deseadas, debido a la mezcla de frecuencias.

\end{itemize}

Además, a partir de los resultados de las pruebas de laboratorio, se puede concluir que:

\begin{itemize}
  \item Respecto a las \textbf{pruebas de reloj y filtrado}, se diseñó e implementó filtros de tipo \textit{combline} de tres etapas, relativamente sencillos de construir. Se lograron pérdidas de apenas 0.8 dB y con un ancho de banda de 31 MHz (Figura \ref{senal_completa_resp_frec}). Ese filtro es el usado en la etapa de generación de señal de referencia sinusoidal de 420 MHz, que nace de una onda cuadrada y que elimina las componentes superiores para entregar una señal sinusoidal (Figura \ref{420_filtrado}).

  \item Respecto a las \textbf{\textit{up converter}}, utilizando el filtro \textit{combline} de 5 etapas se logró atenuar la señal imagen de 395 MHz -30 dB (Figura \ref{filtro_445_abrupto}). Ajustes adicionales del filtro permitieron una mayor atenuación. Filtros de este tipo, conectados en serie, permiten además mayores atenuaciones.

  \item Respecto a las \textbf{pruebas de reloj y filtrado}, con un filtro pasabajo y utilizando una señal de simulación basada en pulsos de $\tau = 0.2~ \mu$s o señal cuadrada al 50\% de ciclo de trabajo, se pudo obtener un espectro alrededor de 25 MHz con los lóbulos distanciados a 2.5 MHz (Figura \ref{25_filtrado_down}). Se nota que la forma de onda es similar a la modulada originalmente (Figura \ref{25_filtrado_down_osc}).

  \item Finalmente, para verificar la recepción dentro de los parámetros del receptor digital, se procedió a hacer una prueba \textit{copper ball}, es decir, la salida modulada de 445 MHz se bajó en frecuencia a 25 MHz, se atenuó y se envió al receptor digital. Se simuló un desplazamiento de m~s$^{-1}$ a 2.1 km de altura. Se pudo obtener el pulso perfectamente definido en el espectro en frecuencia (Figura \ref{resultado_jars_espectro}).


\end{itemize}

\subsection*{Recomendaciones}

\begin{itemize}

  \item Se recomienda especial cuidado en definir los niveles de las señales, en especial al realizar la mezcla y el filtrado. Niveles inadecuados pueden saturar las siguientes etapas o reducir significativamente la sensibilidad del sistema.

  \item Respecto al sistema en general, se recomienda hacer una revisión del estado de arte de los sistemas digitales de radio basados en software o SDR (\textit{Software Defined Radio}.) Por ejemplo, los sistemas USRP (\textit{Universal Software Radio Peripheral}) están liderando las implementación de sistemas de radio ya que llevan el esfuerzo de diseño a escribir flexibles programas de software en vez de diseñar un hardware intrínsecamente menos flexible y de mayor costo.

  \item Respecto a los filtros en UHF, es recomendable hacer una exploración de los filtros en UHF. Aunque se ha utilizado filtros \textit{combline}, no se ha podido encontrar un marco teórico preciso, más bien el proceso de diseño se basó en iteracciones y ajustes, es decir, fue un proceso empírico. Un marco teórico más preciso podría revelar un mejor procesos de diseño

  \item Respecto al diseño electrónico en radio frecuencia, es recomendable explorar la posibilidad de diseñar los circuitos en vez de hacer compras a proveedores. El diseño electrónico en radio frecuencia, aunque es algo más complejo, puede devenir en el desarrollo de tecnología local y también en menores costos. 

\end{itemize}
