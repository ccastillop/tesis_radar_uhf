
\section{Problema de estudio}
	\subsection{Contexto}

	Perú se encuentra en constante riesgo debido a fenómenos atmosféricos tan inusuales como los vientos fuertes en aeropuertos de la selva, o tan catastróficos como el Fenómeno del Niño de 1983 y 1998 que afectaron en especial la costa, o relativamente periódicos como las lluvias torrenciales, aludes y huaycos en la sierra. Por lo tanto existe la necesidad de estudiar y conocer el comportamiento de los vientos y precipitaciones.

	El \gls{roj} es un centro de investigación científica que ha proporcionado información en base a ondas de radio y radares que han sido relevantes para el desarrollo de la física ionosférica, física del plasma y dinámica atmosférica. También ha generado conocimiento científico relativo a la investigación de la \gls{troposfera}.

	En junio del 2014 el \gls{roj} presentó diversos proyectos de innovación tecnológica al \gls{fincyt}. En noviembre del mismo año el \gls{fincyt} comunicó al \gls{roj} la aprobación de esos proyectos entre los cuales estaba el diseño y construcción radar perfilador de vientos que opera en las frecuencias \gls{uhf} para el estudio de la capa límite y precipitaciones en el territorio peruano.

	\subsection{Definición del problema de estudio}

	Según el Servicio Nacional de Meteorología e Hidrología del Perú (SENAMHI, 2010 \cite{senahmi2010}), tres son los factores que determinan básicamente el clima del Perú: la situación del país en la zona intertropical, las modificaciones de altura que introduce la Cordillera de los Andes y la Corriente Peruana o de Humboldt.

	Esos factores junto con la extensión del territorio peruano, variedad de climas y micro climas, así como las situaciones climáticas extremas ponen al país en situaciones vulnerables. Por ejemplo: durante el Fenómeno del Niño de 1983 y 1998 se produjeron lluvias, inundaciones, derrumbes y huaycos que generaron pérdidas humanas y económicas.

	Además, las tormentas y turbulencias en los aeropuertos peruanos pueden poner en riesgo la seguridad antes, durante y después del despegue. Si a eso le sumamos la poca inversión pública y privada en el monitoreo de las condiciones climáticas, nos encontramos ante 2 problemas:

	\begin{enumerate}[1.]
		\item Falta de conocimiento de las condiciones climáticas instantáneas locales, con alta resolución espacial y temporal. Por ejemplo: la magnitud y dirección del viento, así cantidad y tipo de precipitación.
		\item La escasez de bases de datos de condiciones climáticas, que dificulta la precisión de los modelos meteorológicos locales.
	\end{enumerate}

	Una solución es implementar un radar para monitoreo de vientos y lluvias a bajo costo que pueda instalarse en distintas partes del país. Este radar perfilador de vientos en \gls{uhf} sólo apunta de manera vertical. Scipión \cite{scipion2011characterization}); Doviak \& Zrnic \cite{Doviak1992Doppler}, Cohn \cite{cohn1995radar}; Hocking \cite{hocking1983extraction}; White \cite{white1997radar} concluyen que aplicando técnicas de interferometría (\textit{Space Antenna}) se puede medir la turbulencia de vientos en la atmósfera.

	Además, aplicando técnicas de procesamiento de señales se puede detectar las precipitaciones donde se encuentren presentes. Finalmente, el radar puede entregar estimados de los vientos y precipitaciones cada 5 minutos y hasta una altura de 10 km.


	\subsection{Alcance del proyecto}

	La presente tesis busca describir el diseño de las etapas de generación de señal, conversión de frecuencia, amplificación, filtrado y sincronización de un radar perfilador de vientos que opera en \gls{uhf}.

	El diseño de las antenas de recepción y transmisión, la amplificación de alta potencia de radio frecuencia, la etapa de selección o conmutación de antenas, así como la adquisición y procesamiento de señales y la implementación final no están contempladas en la presente tesis.

	\subsection{Objetivos generales y específicos}

	Objetivo general:

	\begin{itemize}
		\item Analizar y diseñar los módulos generación de señal, módulos de conversión de frecuencia, amplificación y filtrado tanto para transmisión como para recepción, y módulo de señales de reloj para un radar que opera 445 MHz
		\item Adaptar los conocimientos y tecnologías del sistema del \gls{roj}, que opera a 50 MHz, y diseñar los equipos para trabajar a 445 MHz.
	\end{itemize}

	Objetivos específicos:

	\begin{itemize}
		\item Recoger los requisitos y especificaciones.
		\item Determinar qué equipos del \gls{roj} van a ser adaptados.
		\item Establecer un diagrama de bloques donde se describan las etapas del sistema.
		\item Determinar los parámetros y especificaciones de las señales de reloj y de radio frecuencia (RF).
		\item Implementar pruebas de laboratorio para probar el diseño de las etapas de generación, conversión de frecuencia, amplificación y filtrado tanto de transmisión como de recepción de las señales de RF.
		\item Realizar los ajustes para que los niveles de señal sean los correctos tanto para la etapa de transmisión, potencia y antena como para la etapa de recepción, adquisición y procesamiento de señales.
	\end{itemize}

\section{Introducción a los sistemas de radar}
	\subsection{Definición y aplicaciones}

El acrónimo \textit{radar} fue sugerido por la Fuerza Naval de EE.UU. en noviembre de 1940 como  \textit{Radio Detecting And Ranging} y se refiere a equipos diseñados para la detección y alcance de objetivos mediante ondas de radio. (Doviak \& Zrnic, 1993 \cite{Doviak1992Doppler})

Según Skolnik, 1990 \cite{skolnik1990radar}, el concepto de radar es relativamente simple, sin embargo, su implementación real no lo es. Un radar opera radiando energía electromagnética y detectando los ecos que regresan reflejados por el objetivo. La naturaleza de la señal reflejada provee de información acerca del objetivo. La distancia o rango del objetivo es conocido a partir del tiempo que toma la energía en ir al objetivo y regresar. El ángulo puede obtenerse mediante antenas directivas, aquellas que concentran la energía en un haz angosto. Si el objetivo se mueve, el radar puede derivar su velocidad mediante el desplazamiento en la frecuencia o efecto Doppler.

Doviak \& Zrnic \cite{Doviak1992Doppler} menciona a Nikola Tesla como el primero en describir el radar:
“Cuando emitimos un sonido y escuchamos el eco en respuesta, sabemos que el sonido de la voz debió haber alcanzado un muro distante, o limite, y ésta debió haber sido reflejada. De la misma manera una onda eléctrica es reflejada ... nosotros podemos determinar la posición relativa o curso de un objetivo en movimiento tal como un buque en el mar, la distancia viajada por éste, o su velocidad ....”

En 1904, en Alemania se realizaron las primeras pruebas de detección de objetivos. Se utilizaron ondas de radio de entre 40 y 50 m de longitud. Sin embargo, para medir la distancia de un objetivo fue necesario desarrollar transmisores pulsados y receptores de ancho de banda amplio.

En 1926 la \textit{Naval Research Laboratory} obtuvo la primera medición de distancia mediante ondas de radio pulsadas al emitir ondas de 71.3 m y recibir el eco a 150 km de altura sobre la tierra, comenzando así el desarrollo de tecnología de radar para investigar la atmósfera. Sin embargo, en 1935 la \textit{Commitee for the Scientific Survey of Air Defense} de Inglaterra impulsó el desarrollo de radares para la detección de aviones enemigos, utilizando los conocimientos adquiridos por los científicos atmosféricos.

Robert A. Watson-Watt, uno de los convocados por la \textit{Naval Research Laboratory}, había trabajado en un sistema para generar alertas tempranas de tormentas. Para Julio de 1935 Sir Watson-Watt había logrado demostrar la detección y alcance de aviones mediante ondas de radio, proporcionando a los aviadores británicos de alertas tempranas ante los aviones alemanes. (Doviak \& Zrnic, 1993 \cite{Doviak1992Doppler})

Recién en 1942, en Londres, se logró detectar y medir la distancia de objetivos utilizando ondas continuas moduladas en frecuencia. La frecuencia de la señal producto de la mezcla entre la onda emitida y reflejada era proporcional a la distancia al objeto.

Durante la Segunda Guerra Mundial los radares basados en efecto Doppler fueron desarrollados para discriminar los objetos de los ecos del mar y la tierra. En 1953 el radar pulsado de efecto Doppler se comenzó a utilizar en mediciones meteorológicas tales como el estudio de precipitaciones y vientos y para la alerta temprana de tormentas y huracanes.

La técnica para la observación de ecos generados en el aire claro de la estratósfera y la mesósfera fue desarrollada en el \gls{roj}, Lima, Perú, en el año 1972 por Rondld F. Woodman y Alberto Guillen (Woodman \& Guillen, 1974 \cite{woodman1974radar}). La técnica de Woodman y Guillen, llamada radar MST(\textit{Mesosphere, Stratosphere, and Troposphere}) por su acrónimo en inglés, mide las desviaciones Doppler con resolución espacial del orden de 150 m. Woodman \& Guillen (1974)\cite{woodman1974radar} señalan que, aunque el radar de Jicamarca, por diseño, no podía detectar ecos de la baja atmósfera (menos de 17 km), sí logra detectarlos en la estratósfera y mesósfera.

Según Córdova, 2006\cite{cordova2006} y Van Zandt, 2006 \cite{van2000brief}, los radares MST o “radares de aire claro” se han convertido en la técnica más potente para el sondeo de la atmósfera y ha contribuido significativamente a las mediciones de los vientos y a las investigaciones de la dinámica atmosférica tales como las ondas gravitatorias, turbulencias y sistemas convectivos.

	\subsection{Partes de un radar}

Según Skolnik \cite{skolnik1990radar}, un radar mono-estático es aquel que utiliza una sola antena para transmitir y recibir. Las partes básicas de un radar monoestático son mostradas en la Figura \ref{radar_basico}.

El radar tiene dos estados: estado de transmisión y estado de recepción. La generación de una señal de referencia y de control de estado T/R (transmisión o recepción) está controlada por la unidad de sincronismo y control.

En el estado de transmisión la señal de radar, mayormente un tren repetitivo de pulsos cortos de radio frecuencia, es generada y amplificada en el transmisor y radiada al espacio por la antena. El conmutador T/R conecta la antena al transmisor.

El radar luego pasa a estado de recepción. Las ondas emitidas por la antena alcanzan el objetivo y éste refleja una pequeña porción de regreso a la antena. Un amplificador de bajo ruido amplifica la señal recibida por la antena para luego ser recolectada por un sistema de procesamiento.

\begin{figure}[h]
\centering
\includegraphics[scale=0.5]{images/RadarBasico.eps}
\caption{Partes básicas de un radar mono-estático. La comparación entre la señal de referencia y la recibida ofrece información de posición y velocidad del objetivo.}
\label{radar_basico}
\end{figure}

El sistema de procesamiento utiliza la señal de referencia y con algoritmos digitales la combina con la señal recibida por la antena para obtener información de magnitud y fase en el dominio del tiempo. Luego las muestras digitales son procesadas para obtener información en la frecuencia mediante el cálculo del espectro empleando la \gls{dtft}.

Skolnik \cite{skolnik1990radar} señala que el tiempo en que llega la señal reflejada por el objetivo entrega información de posición. Los desplazamientos en la frecuencia de la señal recibida con respecto a la referencia ofrecen información de velocidad debido al efecto Doppler.

	\subsection{Frecuencia de transmisión}\label{frecuencia_transmision}

Cualquier dispositivo que detecta y localiza objetivos con energía electromagnética, sin importar la frecuencia, puede ser catalogado como radar, indica Skolnik \cite{skolnik1990radar}. Los radares han operado desde algunos mega Hertz hasta la región ultravioleta del espectro. Aunque el principio de operación es el mismo, en la práctica la implementación es ampliamente variada.

Los primeros radares británicos anteriores a la Segunda Guerra Mundial usaban ondas \gls{hf} debido a limitaciones técnicas que fueron superadas en los años 1930, donde se desarrollaron radares en \gls{vhf} y \gls{uhf}. Estos radares fueron usados para la detección temprana de aviones enemigos y navegación, entre otros usos.

Durante la Segunda Guerra Mundial se desarrollaron radares en la región de las microondas \gls{shf} y se utilizaron tanto para la detección de objetos enemigos como para el control y dirección de misiles, navegación, aviso meteorológico e inclusive por la policía de tránsito para el control de velocidad.

Skolnik \cite{skolnik1990radar} también señala que conforme la frecuencia aumenta, en especial en la región de las microondas, la atenuación por los gases de la atmósfera y del vapor de agua aumenta también. Sin embargo, se logran resolución angular y espacial más precisa debido a que menores longitudes de onda permiten utilizar antenas más pequeñas o concentrar el haz en ángulos más angostos.

Chilson et al (1993) \cite{chilson1993observations} concluyen que las señales de retorno en \gls{uhf} ($\lambda \approx 70$ cm) son más sensibles a precipitaciones, mientras que las señales de  \gls{vhf} ($\lambda \approx  6$ m) son sensibles tanto a precipitaciones como a turbulencias. Por esa razón ambas frecuencias sería adecuadas para detectar ambos fenómenos.

Scipión \cite{scipion2011characterization}); Doviak \& Zrnic \cite{Doviak1992Doppler}, Cohn \cite{cohn1995radar}; Hocking \cite{hocking1983extraction}; White \cite{white1997radar} precisan que, para estudiar la capa de la atmósfera cercana a la Tierra, o capa límite, se utilizan radares \gls{uhf} cercanos a 1 GHz. Por esa razón los radares \gls{uhf}, o \textit{boundary layer radars(BLR)} son más usados para los estudios de aire claro. La resolución típica de estos radares es de 100 m.

Para el radar descrito en la presente tesis se seleccionó la frecuencia \gls{uhf} de entre 400 y 500 MHz debido a las razones antes citadas, a la factibilidad de fabricar localmente antenas para esa longitud de onda, a la movilidad de estas antenas y debido a la tecnología y equipos disponibles en el \gls{roj}.

Previamente los ingenieros del \gls{roj} determinaron, mediante estudios en campo en la banda de 400 a 500 MHz, que la frecuencia que estaba libre de interferencias y se el Ministerio de Transportes y Comunicaciones permitía para este tipo de operación era 445 MHz ($\lambda = 66$ cm).

En la Figura \ref{radar_biestatico} se muestra el radar modificado para utilizar un arreglo de antenas de transmisión y tres arreglos de antenas de recepción. Eso implica utilizar tres canales independientes de recepción y de adquisición de datos. Estos radares son llamados cuasi-mono-estáticos(Scipión \cite{scipion2011characterization}); Doviak \& Zrnic \cite{Doviak1992Doppler}, Cohn \cite{cohn1995radar}; Hocking \cite{hocking1983extraction}; White \cite{white1997radar}). Nótese la necesidad de utilizar conversión de frecuencia para obtener la señal RF de 445 MHz.

\begin{figure}[h]
\centering
\includegraphics[scale=0.6]{images/RadarBasico_doble_antena.eps}
\caption{Radar bi-estático con tres antenas espaciadas. El arreglo de antenas permitiría utilizar interferometría y el análisis espectral permitiría discriminar precipitaciones de ecos de aire-claro.}
\label{radar_biestatico}
\end{figure}

\subsection{Radar con antenas espaciadas}

Briggs et al. (1950) \cite{briggs1950} señala que utilizar un radar con antenas separadas a una distancia conocida permite utilizar técnicas de inteferometría para conocer las propiedades del fenómeno atmosférico con mayor precisión. Por ejemplo, mediante interferometría se halla no sólo la velocidad del viento, también la dirección en tres dimensiones del objetivo.

Por esa razón se opta por diseñar un radar basado en cuatro arreglos de antenas; un arreglo para transmisión y tres arreglos  para recepción. Este conjunto de antenas permitiría:

\begin{enumerate}[1.]
\item Los arreglos de antenas, al interferirse entre sí, permiten enfocar y concentrar el haz electromagnético en un ángulo pequeño.
\item No sería necesario un dispositivo de conmutación de transmisión y recepción. Los T/R suelen ser dispositivos de alta potencia y por lo tanto costosos.
\item Los tres arreglos de antenas de recepción, separados a una distancia conocida, conformarían un dispositivo de interferometría para conocer el vector de velocidad del objetivo de manera tridimensional.
\item Empleando técnicas de procesamiento espectral se podrá discriminar los ecos del aire-claro de los de precipitación, lo que nos permitirá estimar la cantidad de precipitación acumulada donde éste se encuentre presente.
\end{enumerate}

	\subsection{Conversión de frecuencia}

Uno de los requisitos para implementar el radar \gls{uhf} a 445 MHz era aprovechar los equipos y tecnologías desarrollados en el \gls{roj}. Sin embargo, los equipos del \gls{roj}, en especial el generador de señales RF, trabaja hasta 100 MHz, siendo usual programarlo con frecuencias entre 25 y 65 MHz.

Para generar señales de 445 MHz se decide utilizar conversores de frecuencia o \textit{Up Converters}. Esa señal se obtiene mezclando una señal de 420 MHz con otra de 25 MHz y, luego de un filtro pasabanda, se obtiene la señal de 445 MHz. Eso planteó la necesidad de generar la señal sinusoidal de referencia de 420 MHz. Puede verse el diagrama de bloques del \textit{up-converter} en la Figura \ref{radar_biestatico}.

El generador de la señal de 25 MHz es el \textit{Direct Digital Syntetizer(\gls{dds})} o sintetizador digital directo (ROJ, 2010 \cite{roj_ad9854}), basado en el chip AD9854 de \textit{Analog Devices} y desarrollado en el \gls{roj}. Este sistema además provee de modulación por código binario en fase (BPSK) y puede ser programado en distintas frecuencias a precisión.

De manera similar, para la etapa de recepción se convierte la frecuencia de 445 MHz, recibida en la antena, a 25MHz utilizando la señal de referencia de 420 MHz. En la Figura \ref{radar_biestatico} se muestra el mezclador como \textit{up-converter} y el filtro pasa banda previo a la transmisión.

Para generar la señal de 420 MHz se utiliza el generador de frecuencias de referencia o \textit{clock reference}. Este dispositivo, también desarrollado en el \gls{roj}, se basa en el chip AD9548 de \textit{Analog Devices} que permite obtener señales de reloj desde 1 kHz hasta 450 MHz (ROJ, 2012 \cite{roj_frecuency_ref}). Éste generador se sincroniza a un reloj maestro de 1 pps (pulso por segundo) y otra señal de 10 MHz, ambos obtenidos desde un receptor \gls{gps} (\textit{Global Positioning System}).

Para finalizar, en el presente capítulo se desarrolló el problema de estudio que consiste en implementar un radar perfilador de vientos, con enfoque en diseñar las etapas de conversión de frecuencia para transmisión y recepción. Además se hizo una introducción a los radares de aire-claro y para medición de precipitaciones. Este radar serviría para entender mejor los vientos y precipitaciones en el territorio peruano.
