Con el fin de validar y ajustar el diseño se procedió a ensamblar los componentes en el laboratorio de Investigación, Desarrollo e Innovación del \gls{roj} (IDI). El laboratorio tiene instrumentales tales como osciloscopios, analizadores de espectro, generadores de señales, fuentes de poder, multímetros, cables, conectores y terminaciones. Además están disponibles atenuadores, mezcladores, divisores de potencia y amplificadores con especificaciones.

\section{Prueba de mezcla de señales}

En primer lugar se procede a validar la mezcla de señales, que serán luego utilizadas tanto para el \textit{up-converter} como para el \textit{down-converter}. Para simular la señal a transmitir, se genera una señal de RF de 25 MHz y se modula en BPSK con una onda cuadrada de 2.5 MHz y de 50\% de ciclo de trabajo.

Nótese que 2.5 MHz corresponden a un periodo de 0.4 $\mu$s. Ajustando el ciclo de trabajo (\textit{duty cycle}) de la onda cuadrada al 50\% se simulan pulsos binarios [1010...] que corresponden a cambios de fase de 0$^\circ$ y 180$^\circ$. El ancho de estos pulsos es de 0.2 $\mu$s, es decir, 20\% superior a la especificación.

Con esta prueba se facilita la visibilidad en el osciloscopio y en el analizador de espectro de frecuencias, ya que la modulación toma exactamente 5 periodos de la portadora por cada dígito binario.

En la Figura \ref{senal_completa_01} se muestra una primera prueba donde la señal modulada en BPSK se mezcló, con un mezclador balanceado, con una señal de 420 MHz generada por el generador de frecuencias de referencia, previamente filtrada mediante un filtro basado en conectores coaxiales (\textit{notch filter}).

Luego, la señal resultante del mezclador se atenuó y se inyectó a otro mezclador junto con la referencia de 420 MHz. Nótese cómo se respeta el cambio de fase en la señal y cómo se ve ésta en el osciloscopio (ver Figura \ref{resultado_respuesta_bpsk}).

Nótese  que en esta prueba se utilizó un artilugio a modo de filtro a 420 MHz. Mediante cables y conectores es posible crear un \textit{notch filter} que utiliza una característica de las líneas de transmisión de tamaño $\lambda / 4$. Cuando una línea de transmisión es de un cuarto de longitud de onda y uno de los extremos se mantiene abierto, el otro extremo se comporta como un corto-circuito.

\begin{figure}[H]
\centering
\includegraphics[scale=0.36]{images/RutaSenal_completa_bn.jpg}
\caption{Primera prueba de señal completa, antes de implementar filtros. Se utiliza un generador de señales a 25 MHz, un modulador BPSK, un divisor de potencia (\textit{splitter}), un Atenuador ajustable y dos mezladores balanceados marca Mini-circuits\textsuperscript{\textregistered}}
\label{senal_completa_01}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[scale=0.45]{images/TEK0001_bn.jpg}
\caption{RF de 25 MHz modulado en BPSK por una onda cuadrada de 2.5 MHz. La señal es mezclada con una de 420 MHz. Luego de atenuarse, se mezcla nuevamente con una de 420 MHz y luego de un filtro pasabajos se obtiene la respuesta del último canal.}
\label{resultado_respuesta_bpsk}
\end{figure}

Si hacemos que esas líneas tengan el tamaño necesario para eliminar el segundo y tercer armónico de una señal de 420 MHz (840 MHz y 1.25 GHz) es posible obtener una señal limpia sinusoidal de 420 MHz. Se encontró que los adaptadores BNC tienen un tamaño que elimina esas componentes por lo que fueron utilizados en esta primera prueba.

En la siguiente etapa se concentró el esfuerzo en desarrollar filtros con mayor precisión y así diseñar el sistema con anchos de banda determinados.

\section{Reloj y filtrado de la señal de referencia en 420 MHz}

Una vez concluida la primera prueba se concentró el esfuerzo en obtener una señal de referencia de 420 MHz lo más pura posible. Para ello fue necesario diseñar y realizar un filtro a 420 MHz. Se utilizó el software \textit{Ansoft Designer SV (Student Version)} que entrega las dimensiones de los componentes del filtro.

\begin{figure}[H]
\centering
\includegraphics[scale=0.35]{images/Filtro_420_respuestafrec_bn.jpg}
\caption{Respuesta en frecuencia del filtro \textit{combline} a 420 MHz. El punto M1 indica la frecuencia central, con una pérdida de 0.8 dB. El ancho de banda (punto M2) es de 31 MHz. }
\label{senal_completa_resp_frec}
\end{figure}

El resultado final puede verse en la Figura \ref{420_filtrado}. Nótese que el generador de señales de referencia introduce además otras componentes debido a interferencias entre sus canales. Gran parte de esas interferencias fueron eliminadas por el filtro de 420 MHz.

\begin{figure}[H]
  \centering
  \begin{subfigure}[b]{0.71\textwidth}
    \includegraphics[width=1\linewidth]{images/420QUAD_13DBM_bn_a.png}
    \caption{Antes del filtro}
  \end{subfigure}
  \begin{subfigure}[b]{0.71\textwidth}
    \vspace{1.5em}
    \includegraphics[width=1\linewidth]{images/420QUAD_13DBM_bn_b.png}
    \caption{Después del filtro}
  \end{subfigure}
  \caption{Comparación entre el espectro de una onda cuadrada de 420 MHz, antes del filtro \textit{combline} y después de ser filtrada}
  \label{420_filtrado}
\end{figure}


\section{Resultados del \textit{up-converter}}

Se procedió a realizar un filtro a 445 MHz que atenúe los espurios de la mezcla de 420 MHz con la señal RF BPSK de 25 MHz. Se consideró necesario diseñar un filtro con corte abrupto de cinco elementos para atenuar al máximo la imagen de 395 MHz y la portadora de 420 MHz.

\begin{figure}[H]
\centering
\includegraphics[scale=0.28]{images/Filtros_afinados_bn.jpg}
\caption{Dos Filtros \textit{combline} de 445 MHz calibrados fabricados en el \gls{roj}. El más grande es de tres elementos. El más pequeño es de cinco elementos con corte más abrupto}
\label{filtros_afinados}
\end{figure}

El la Figura \ref{filtro_445_abrupto} se muestra la respuesta del filtro con corte abruto. El objetivo era obtener -40 dB de atenuación a 420 MHz y -60 dB para 400 MHz.

\begin{figure}[H]
\centering
\includegraphics[scale=0.38]{images/Filtro_445_abrupto_bn.jpg}
\caption{Respuesta del filtro de 445 MHz con corte abrupto. Nótese la caída a 420 MHz (-39 dB, punto M2) y a 400 MHz (-65 dB, punto M1).}
\label{filtro_445_abrupto}
\end{figure}

Se procedió a conectar el filtro a la salida del mezclador del \textit{up-converter} y, luego de aplicar una RF de 25 MHz modulada en BPSK a 2.5 MHz y la señal sinusoidal de 420 MHz se obtuvieron los siguientes resultados

\begin{figure}[H]
  \centering
  \begin{subfigure}[b]{0.71\textwidth}
    \includegraphics[width=1\linewidth]{images/Fitro_445_resultados_bn_a.jpg}
    \caption{Antes del filtro}
  \end{subfigure}
  \begin{subfigure}[b]{0.71\textwidth}
    \vspace{1.5em}
    \includegraphics[width=1\linewidth]{images/Fitro_445_resultados_bn_b.jpg}
    \caption{Después del filtro}
  \end{subfigure}
  \caption{Salida del \textit{up-converter} antes y después del filtro. Nótese la atenuación en la señal de imagen de 395 MHz.} de alrededor de -30 dB.
  \label{filtro_445_resultados}
\end{figure}

\section{Recepción, \textit{down converter} y filtrado}

A continuación se procedió someter a pruebas el mezclador y el filtro pasa-bajo para obtener el \textit{down converter}. Para ello se utilizó un divisor de potencia o \textit{splitter} que dividía la referencia de 420 MHz y se utilizaba una parte para la mezcla con la señal que simulaba la de la antena (Figura \ref{resultado_ruta_senal_final}).

El resultado del sistema ensamblado se puede ver en el analizador de espectros (Figura \ref{25_filtrado_down}). Se observa el comportamiento de la señal luego de la mezcla, donde aparecen lóbulos que coinciden con la modulación BPSK, es decir, están distanciados a 2.5 MHz. Es decir, el espectro y la información original ha sido trasladada desde la frecuencia de 445 MHz a la de 25 MHz.

En la Figura \ref{25_filtrado_down} se muestra el resultado de la mezcla y posterior filtrado. Se optó por realizar la modulación de fase algo más allá de la frecuencia máxima, con un $\tau = 0.2~ \mu$s o señal cuadrada al 50\% de ciclo de trabajo. En el espectro de frecuencia los lóbulos se separan unos de otros 2.5 MHz y el receptor es capaz de detectar hasta 3 lóbulos por lado, es decir, aproximadamente 25 MHz de ancho de banda.

En la Figura \ref{25_filtrado_down_osc} se muestra las salidas en el osciloscopio. La señal del canal 3 es la resultante de la mezcla y filtrado.

\begin{figure}[H]
\centering
\includegraphics[scale=0.34]{images/Ruta_senal_final_bn.jpg}
\caption{Ensamblaje de la ruta de señal completa, incluyendo los filtros, el mezclador del \textit{up-converter} y el mezclador del \textit{down converter}. Nótese la respuesta en el tiempo en el osciloscopio así como la respuesta en frecuencia en el analizador de espectros.}
\label{resultado_ruta_senal_final}
\end{figure}

\begin{figure}[H]
  \centering
  \begin{subfigure}[b]{0.71\textwidth}
    \includegraphics[width=1\linewidth]{images/resultado_final_frecuencia_bn_a.png}
    \caption{Espectro de 0 Hz a 1 GHz}
  \end{subfigure}
  \begin{subfigure}[b]{0.71\textwidth}
    \vspace{1.5em}
    \includegraphics[width=1\linewidth]{images/resultado_final_frecuencia_bn_b.png}
    \caption{Espectro de 0 Hz a 50 MHz}
  \end{subfigure}
  \caption{Salida del \textit{down converter} después del filtro pasa bajo y detalle de la salida ampliada. Nótese los lóbulos producidos por la modulación BPSK de 2.5 MHz $\tau = 0.2 \mu s $}
  \label{25_filtrado_down}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.40]{images/TEK0005_bn.jpg}
\caption{Detalle de las señales modulante, RF modulada y salida del \textit{down converter} después del filtro pasa bajo. Se ha intercalado los filtros necesarios para la correcta de-modulación.}
\label{25_filtrado_down_osc}
\end{figure}

Como paso final, se procedió a realizar una prueba \textit{copper ball} a 25 MHz con un diferencial de frecuencia tal que simule un objeto que se mueve hacia el radar a 10 m~s$^{-1}$. Esto, a la frecuencia del radar (445 MHz) significa un desplazamiento de 30 Hz aproximadamente.

Para verificar que el sistema \gls{jars} pueda trabajar a la velocidad necesaria para la resolución requerida (37.5 m o 0.25 $\mu$s), se introdujo la señal a los cuatro canales simultáneamente. Al conectarse los canales a la señal con cables de distinto tamaño se logra obtener desfases como se ve en la Figura \ref{resultado_jars_tiempo}. Además el pulso se ajustó para que aparezca a 2 km de altura.

En la Figura \ref{resultado_jars_espectro} se muestra el resultado de la prueba \textit{copper ball} en el dominio de las frecuencias. El eje vertical es traducido a rango al cambiar el tiempo en distancia debido a la velocidad de la luz. La desviación en frecuencia por el efecto Doppler se represente en el eje horizontal. Nótese el pulso ubicado a 2 km de altura.

\begin{figure}[H]
\centering
\includegraphics[scale=0.35]{images/tiempo_JARS_bn.png}
\caption{Resultado de la señal al ser introducida al sistema \gls{jars} en cuatro canales, vista en el dominio del tiempo, traducido a rango por la constante de velocidad de la luz. Nótese que la misma señal se introduce a los cuatro canales. Existen desfases debido a que los cables por canal no son del mismo tamaño. El pulso se ajustó para que aparezca a 2 km de altura.}
\label{resultado_jars_tiempo}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.60]{images/spc_2016145_0248_bn.png}
\caption{La señal de prueba vista en el dominio de la frecuencia. Un punto oscuro muestra la señal a 10 m~s$^{-1}$ y a 2 km de altura.}
\label{resultado_jars_espectro}
\end{figure}

Finalmente, el capítulo presente desarrolló las pruebas que se hicieron para validar los conversores de frecuencia, filtros y la referencia de frecuencias. Fue necesario trabajar con los equipos e instrumentos del \gls{roj} tales como generadores de señales, analizadores de espectro, osciloscopios y, además, fue necesario conocer el funcionamiento de los sistemas \gls{jars}, \gls{dds} y \gls{cr}.
