class Float
  def round_5
    (self / 5).round * 5
  end
end

class Radar
  L = 299792458 # velocidad luz m/s
  K = 1.38064852 * 10 ** -23 #m2 kg s-2 K-1  #Boltzmann constant

  def r_jars(arg={})
    n_jars = arg[:n_jars] ||= 32
    f_jars = arg[:f_jars] ||= 25*10**6#mhz
    f_c_jars = arg[:f_c_jars] ||= 60*10**6#mhz
    r_jars = f_jars * 1.0 * 2**n_jars / f_c_jars
  end

  def r_dds(arg = {})
    r_jars = arg[:r_jars] ||= 1789569705
    n_jars = arg[:n_jars] ||= 32
    n_dds = arg[:n_dds] ||= 48
    m_dds = arg[:m_dds] ||= 5
    f_c_jars = arg[:f_c_jars] ||= 60*10**6#mhz
    f_c_dds = arg[:f_c_dds] ||= 60*10**6#mhz
    r_dds = 1.0 * r_jars * f_c_jars * 2**(n_dds-n_jars) / (f_c_dds * m_dds)
  end

  def r_dds2(arg = {})
    f_dds = arg[:f_dds] ||= 25*10**6#mhz
    n_jars = arg[:n_jars] ||= 32
    n_dds = arg[:n_dds] ||= 48
    m_dds = arg[:m_dds] ||= 5
    f_c_dds = arg[:f_c_dds] ||= 60*10**6#mhz

    r_dds = 1.0 * ( f_dds * 1.0 * 2**n_jars ) * 2**(n_dds-n_jars) / (f_c_dds * m_dds)
  end

  #23456248037375

  def f_dds(arg = {})
    n_dds = arg[:n_dds] ||= 48
    m_dds = arg[:m_dds] ||= 5
    r_dds = arg[:r_dds] ||= 23456248037376
    f_c_dds = arg[:f_c_dds] ||= 60*10**6#mhz
    f_dds = f_c_dds * m_dds * r_dds * 1.0 / 2**n_dds
  end

  def v_d(arg = {})
    f_radar = arg[:f_radar] ||= 445.0*10**6
    lamba = arg[:lamba] ||= L/f_radar
    f_d = arg[:f_d] ||= -29.687204472635532
    v_d = - lamba * f_d / 2
  end

  def f_d(arg = {})
    f_radar = arg[:f_radar] ||= 445*10**6
    lamba = arg[:lamba] ||= 1.0 * L/f_radar
    v_d = arg[:v_d] ||= 10
    f_d = 2*v_d / lamba
  end

  #ecuación de radar
  def p_r( arg = {} )
    p_t = arg[:p_t] || 5.0*10**3 #potencia transmisión 5,000 mW
    g_t = arg[:g_t] || 24.0 #dB ganancia transmisión
    g_t = 10.0**(g_t/10.0) #, valores naturales
    g_r = arg[:g_r] || 17.0 #dB ganancia recepción, valores naturales
    g_r = 10.0**(g_r/10.0) #, valores naturales
    f_r = arg[:f_r] || 445.0 * 10**6 # 445 MHz frecuencia del radar
    lmbda = arg[:lmbda] || (L * 1.0 / f_r) # lambda radar
    r_0 = arg[:r_0] || 5.0 * 10**3 # rango radar 5 km
    l = arg[:l] || 0.98 # atenuación de cables
    t_w = arg[:t_w] || (0.25 * 10**(-6)) #us radar pulse
    theta_tx =  arg[:theta_tx] || 9.49  # $^\circ$ en grados, angulo apertura de antenas
    theta_tx = theta_tx * Math::PI / 180.0 # conversión a radianes
    theta_rx =  arg[:theta_rx] || 21.07  # $^\circ$ en grados, angulo apertura de antenas
    theta_rx = theta_rx * Math::PI / 180.0 # conversión a radianes

    # eta aire claro
    # η = 0,379 Cn2 λ −1/3,
    c_2_n = arg[:c_2_n] || 6.0*10**(-17) # aire claro
    eta_ac = 0.379 * c_2_n * lmbda**(-1/3)

    # eta metereológico
    z = arg[:z] || 0.0 #dBZ para nubes cumulus
    z = 10.0**(z/10.0) * 10**(-18)
    k_2 = arg[:k_2] || 0.93 # |K|^2 = 0.93 para fase de agua 0 a 20$^\circ$
    eta_met = (Math::PI**5 / lmbda**4) * k_2 * z

    eta_ac = arg[:eta_ac] || eta_ac
    eta_met = arg[:eta_met] || eta_met

    t1 = ( p_t * g_t * g_r * lmbda**2 ) / ( (4 * Math::PI)**3 * r_0**2 * l**2 )
    t2 = L * t_w / 2
    t3 = ( Math::PI * theta_tx * theta_rx ) / ( 8 * Math.log(2) )

    result = {
      pr_ac: 10*Math::log10(t1 * t2 * t3 * eta_ac),
      pr_met: 10*Math::log10(t1 * t2 * t3 * eta_met)
    }

    result
  end

  def p_noise(arg={})
    temp_ref = arg[:temp_ref] || 290 #kelvin
    noise_figure = arg[:noise_figure] || 0.38 #dB, LNA noise figure
    noise_temp = 1.0*arg[:noise_temp] || temp_ref * ( 10 ** (noise_figure/10) - 1 )
    b = arg[:b] || 25.0 * 10 ** 6 #bandwidth 25 MHz.
    10 * Math::log10(K * noise_temp * b / 0.001)
  end

end
